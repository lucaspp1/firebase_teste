import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'package:fire_base_teste/app/modules/aplication/aplication_controller.dart';
import 'package:fire_base_teste/app/modules/aplication/aplication_module.dart';

void main() {
  initModule(AplicationModule());
  AplicationController aplication;

  setUp(() {
    aplication = AplicationModule.to.get<AplicationController>();
  });

  group('AplicationController Test', () {
    test("First Test", () {
      expect(aplication, isInstanceOf<AplicationController>());
    });

  });
}
