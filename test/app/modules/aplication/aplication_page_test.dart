import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:fire_base_teste/app/modules/aplication/aplication_page.dart';

main() {
  testWidgets('AplicationPage has title', (WidgetTester tester) async {
    await tester
        .pumpWidget(buildTestableWidget(AplicationPage(title: 'Aplication')));
    final titleFinder = find.text('Aplication');
    expect(titleFinder, findsOneWidget);
  });
}
