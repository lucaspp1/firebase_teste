// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeBase, Store {
  final _$waitingAtom = Atom(name: '_HomeBase.waiting');

  @override
  bool get waiting {
    _$waitingAtom.context.enforceReadPolicy(_$waitingAtom);
    _$waitingAtom.reportObserved();
    return super.waiting;
  }

  @override
  set waiting(bool value) {
    _$waitingAtom.context.conditionallyRunInAction(() {
      super.waiting = value;
      _$waitingAtom.reportChanged();
    }, _$waitingAtom, name: '${_$waitingAtom.name}_set');
  }

  final _$_HomeBaseActionController = ActionController(name: '_HomeBase');

  @override
  void goToAplication() {
    final _$actionInfo = _$_HomeBaseActionController.startAction();
    try {
      return super.goToAplication();
    } finally {
      _$_HomeBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string = 'waiting: ${waiting.toString()}';
    return '{$string}';
  }
}
