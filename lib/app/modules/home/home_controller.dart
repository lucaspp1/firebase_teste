import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'home_controller.g.dart';

class HomeController = _HomeBase with _$HomeController;

abstract class _HomeBase with Store {
  void initFirebase() {
    Firestore.instance
        .collection('books')
        .document()
        .setData({'title': 'title', 'author': 'author'});
  }

  @observable
  bool waiting = false;

  @action
  void goToAplication() {
    waiting = true;
    Future.delayed(Duration(seconds: 2)).then((_) => goNext());
  }

  void goNext() {
    waiting = false;
    Modular.to.pushNamed("/aplication");
  }
}
