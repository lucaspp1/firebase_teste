import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'aplication_controller.g.dart';

class AplicationController = _AplicationBase with _$AplicationController;

abstract class _AplicationBase with Store {

  @observable
  bool waiting = false;

  @action
  void goToHome() {
    waiting = true;
    Future.delayed(Duration(seconds: 2)).then((_) => goNext());
  }

  void goNext() {
    waiting = false;
    Modular.to.pushNamed("/");
  }
}
