// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'aplication_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AplicationController on _AplicationBase, Store {
  final _$waitingAtom = Atom(name: '_AplicationBase.waiting');

  @override
  bool get waiting {
    _$waitingAtom.context.enforceReadPolicy(_$waitingAtom);
    _$waitingAtom.reportObserved();
    return super.waiting;
  }

  @override
  set waiting(bool value) {
    _$waitingAtom.context.conditionallyRunInAction(() {
      super.waiting = value;
      _$waitingAtom.reportChanged();
    }, _$waitingAtom, name: '${_$waitingAtom.name}_set');
  }

  final _$_AplicationBaseActionController =
      ActionController(name: '_AplicationBase');

  @override
  void goToHome() {
    final _$actionInfo = _$_AplicationBaseActionController.startAction();
    try {
      return super.goToHome();
    } finally {
      _$_AplicationBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string = 'waiting: ${waiting.toString()}';
    return '{$string}';
  }
}
