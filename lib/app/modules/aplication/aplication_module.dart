import 'package:fire_base_teste/app/modules/aplication/aplication_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:fire_base_teste/app/modules/aplication/aplication_page.dart';

class AplicationModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => AplicationController()),
      ];

  @override
  List<Router> get routers => [
        Router('/', child: (_, args) => AplicationPage()),
      ];

  static Inject get to => Inject<AplicationModule>.of();
}
