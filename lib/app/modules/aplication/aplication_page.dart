import 'package:fire_base_teste/app/modules/aplication/aplication_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class AplicationPage extends StatefulWidget {
  final String title;
  const AplicationPage({Key key, this.title = "Aplication"}) : super(key: key);

  @override
  _AplicationPageState createState() => _AplicationPageState();
}

class _AplicationPageState extends State<AplicationPage> {

  AplicationController aplicationController = AplicationController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Observer(builder: (_) {
            return aplicationController.waiting ? CircularProgressIndicator(backgroundColor: Colors.black,) : Text("not Loading");
          }),
          Center(
            child: Text("Go to aplication"),
          ),
          Center(
            child: MaterialButton(onPressed: aplicationController.goToHome, child: Text("Go to")),
          )
        ],
      ),
    );
  }
}
